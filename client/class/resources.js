import * as ex from "excalibur";

const Resources = {
  keen: new ex.ImageSource("../image/keen.png"),
  bot: new ex.ImageSource("../image/bot.png"),
  botRed: new ex.ImageSource("../image/bot-red.png"),
  baddie: new ex.ImageSource("../image/baddie.png"),
  block: new ex.ImageSource("../image/block32x32.png"),
  cylinder_blue: new ex.ImageSource("../image/cylinder-blue32x32.png"),
  npc: new ex.ImageSource("../image/npc.png"),
  jump: new ex.Sound("../sound/jump.wav"),
  hit: new ex.Sound("../sound/hurt.wav"),
  gotEm: new ex.Sound("../sound/gottem.wav"),
};

const keenSpriteSheet = ex.SpriteSheet.fromImageSource({
  image: Resources.keen,
  grid: {
    columns: 11,
    rows: 3,
    spriteWidth: 32,
    spriteHeight: 32,
  },
});

const botSpriteSheet = ex.SpriteSheet.fromImageSource({
  image: Resources.bot,
  grid: {
    columns: 8,
    rows: 1,
    spriteWidth: 32,
    spriteHeight: 32,
  },
});
const botRedSpriteSheet = ex.SpriteSheet.fromImageSource({
  image: Resources.botRed,
  grid: {
    columns: 8,
    rows: 1,
    spriteWidth: 32,
    spriteHeight: 32,
  },
});
const baddieSpriteSheet = ex.SpriteSheet.fromImageSource({
  image: Resources.baddie,
  grid: {
    columns: 6,
    rows: 1,
    spriteWidth: 32,
    spriteHeight: 32,
  },
});

const blocks = {};
blocks["blockSprite"] = Resources.block.toSprite();
blocks["cylinder_blue"] = Resources.cylinder_blue.toSprite();
const npcSprite = Resources.npc.toSprite();
npcSprite.scale = new ex.Vector(2, 2);

const loader = new ex.Loader();
for (const res in Resources) {
  loader.addResource(Resources[res]);
}

export {
  Resources,
  loader,
  keenSpriteSheet,
  botSpriteSheet,
  botRedSpriteSheet,
  baddieSpriteSheet,
  blocks,
  npcSprite,
};
