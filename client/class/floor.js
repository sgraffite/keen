import * as ex from "excalibur";
//import { ExcaliburGraphicsContext } from "excalibur";
import { blocks } from "./resources";

export class Floor extends ex.Actor {
  constructor(x, y, cols, rows) {
    super({
      name: "Floor",
      pos: new ex.Vector(x, y),
      //scale: new ex.Vector(2, 2),
      anchor: ex.Vector.Zero,
      collider: ex.Shape.Box(
        blocks["blockSprite"].width * cols,
        blocks["blockSprite"].height * rows,
        ex.Vector.Zero
      ),
      collisionType: ex.CollisionType.Fixed,
      collisionGroup: ex.CollisionGroupManager.groupByName("platform"),
    });

    this.cols = cols;
    this.rows = rows;

    for (let i = 0; i < this.cols; i++) {
      for (let j = 0; j < this.rows; j++) {
        this.graphics.show(blocks["blockSprite"], {
          anchor: ex.Vector.Zero,
          offset: ex.vec(
            i * blocks["blockSprite"].width,
            j * blocks["blockSprite"].height
          ),
        });
      }
    }
  }
}
