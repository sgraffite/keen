import * as ex from "excalibur";
import { keenSpriteSheet, Resources } from "./resources";
import { Baddie } from "./baddie";

export class Keen extends ex.Actor {
  onGround = true;
  isJumping = false;
  isHurt = false;
  #direction = "right";
  hurtTime = 0;

  set direction(value) {
    this.#direction = value;
  }

  get direction() {
    return this.#direction;
  }

  constructor(x, y) {
    super({
      name: "Player",
      pos: new ex.Vector(x, y),
      collisionType: ex.CollisionType.Active,
      collisionGroup: ex.CollisionGroupManager.groupByName("player"),
      collider: ex.Shape.Box(25, 50, ex.Vector.Half, ex.vec(0, 3)),
    });
  }

  // OnInitialize is called before the 1st actor update
  onInitialize(engine) {
    // Initialize actor

    // Setup visuals
    const hurtleft = ex.Animation.fromSpriteSheet(
      keenSpriteSheet,
      [0, 1, 0, 1, 0, 1],
      150
    );
    hurtleft.scale = new ex.Vector(2, 2);

    const hurtright = ex.Animation.fromSpriteSheet(
      keenSpriteSheet,
      [0, 1, 0, 1, 0, 1],
      150
    );
    hurtright.scale = new ex.Vector(2, 2);
    hurtright.flipHorizontal = true;

    const idleLeft = ex.Animation.fromSpriteSheet(keenSpriteSheet, [4], 800);
    idleLeft.scale = new ex.Vector(2, 2);
    const idleRight = ex.Animation.fromSpriteSheet(keenSpriteSheet, [0], 800);
    idleRight.scale = new ex.Vector(2, 2);

    const left = ex.Animation.fromSpriteSheet(
      keenSpriteSheet,
      [4, 5, 6, 7],
      100
    );
    left.scale = new ex.Vector(2, 2);

    const right = ex.Animation.fromSpriteSheet(
      keenSpriteSheet,
      [0, 1, 2, 3],
      100
    );
    right.scale = new ex.Vector(2, 2);

    const jumpingleft = ex.Animation.fromSpriteSheet(
      keenSpriteSheet,
      //[17, 18, 19, 20, 21],
      [21],
      100
    );
    jumpingleft.scale = new ex.Vector(2, 2);
    const jumpingright = ex.Animation.fromSpriteSheet(
      keenSpriteSheet,
      //[11, 12, 13, 14, 15],
      [15],
      100
    );
    jumpingright.scale = new ex.Vector(2, 2);

    const fallingleft = ex.Animation.fromSpriteSheet(
      keenSpriteSheet,
      [21],
      100,
      ex.AnimationStrategy.Freeze
    );
    fallingleft.scale = new ex.Vector(2, 2);
    const fallingright = ex.Animation.fromSpriteSheet(
      keenSpriteSheet,
      [15],
      100,
      ex.AnimationStrategy.Freeze
    );
    fallingright.scale = new ex.Vector(2, 2);

    // Register animations with actor
    this.graphics.add("hurtleft", hurtleft);
    this.graphics.add("hurtright", hurtright);
    this.graphics.add("idleleft", idleLeft);
    this.graphics.add("idleright", idleRight);
    this.graphics.add("left", left);
    this.graphics.add("right", right);
    this.graphics.add("jumpingleft", jumpingleft);
    this.graphics.add("jumpingright", jumpingright);
    this.graphics.add("fallingleft", fallingleft);
    this.graphics.add("fallingright", fallingright);

    // onPostCollision is an event, not a lifecycle meaning it can be subscribed to by other things
    this.on("postcollision", (evt) => this.onPostCollision(evt));
    //this.on("precollision", (evt) => this.onPreCollision(evt));
  }

  onPreCollision(evt) {
    console.log(evt);
  }

  onPostCollision(evt) {
    // Bot has collided with the Top of another collider
    //console.log(evt.side);
    if (evt.side === ex.Side.Bottom && this.vel.y === 0) {
      this.onGround = true;
    }

    // Bot has collided on the side, display hurt animation
    if (
      (evt.side === ex.Side.Left || evt.side === ex.Side.Right) &&
      evt.other instanceof Baddie
    ) {
      if (this.vel.x < 0 && !this.hurt) {
        this.graphics.use("hurtleft");
      }
      if (this.vel.x >= 0 && !this.hurt) {
        this.graphics.use("hurtright");
      }
      this.isHurt = true;
      this.hurtTime = 1000;
      Resources.hit.play(0.1);
    }
  }

  // After main update, once per frame execute this code
  onPreUpdate(engine, delta) {
    const keyboard = engine.input.keyboard;
    // If hurt, count down
    if (this.hurtTime >= 0 && this.hurt) {
      this.hurtTime -= delta;
      if (this.hurtTime < 0) {
        this.isHurt = false;
      }
    }

    // Reset x velocity
    this.vel.x = 0;

    // Player input
    if (keyboard.isHeld(ex.Keys.Left) || keyboard.isHeld(ex.Keys.A)) {
      this.vel.x = -200;
      this.direction = "left";
    }

    if (keyboard.isHeld(ex.Keys.Right) || keyboard.isHeld(ex.Keys.D)) {
      this.vel.x = 200;
      this.direction = "right";
    }

    // Jump
    if (
      this.vel.y === 0 &&
      this.onGround &&
      (keyboard.isHeld(ex.Keys.Up) ||
        keyboard.isHeld(ex.Keys.Space) ||
        keyboard.isHeld(ex.Keys.W))
    ) {
      this.vel.y = -450;
      this.onGround = false;
      Resources.jump.play(0.1);
      this.graphics.use(`jumping${this.direction}`);
    }

    // Partial jump
    if (
      !this.onGround &&
      this.vel.y < 0 &&
      (keyboard.wasReleased(ex.Keys.Up) ||
        keyboard.wasReleased(ex.Keys.Space) ||
        keyboard.wasReleased(ex.Keys.W))
    ) {
      this.vel.y = this.vel.y / 4;
      this.graphics.use(`jumping${this.direction}`);
    }

    // Change animation based on velocity
    if (this.vel.x < 0 && this.vel.y === 0 && !this.hurt) {
      this.graphics.use("left");
    }
    if (this.vel.x > 0 && this.vel.y === 0 && !this.hurt) {
      this.graphics.use("right");
    }
    if (this.vel.x === 0 && this.vel.y === 0 && !this.hurt) {
      this.graphics.use(`idle${this.direction}`);
    }

    // Falling animation
    if (this.vel.x < 0 && this.vel.y > 0) {
      this.graphics.use("fallingleft");
    }
    if (this.vel.x > 0 && this.vel.y > 0) {
      this.graphics.use("fallingright");
    }
    if (this.vel.x === 0 && this.vel.y > 0) {
      this.graphics.use(`falling${this.direction}`);
    }
  }
}
