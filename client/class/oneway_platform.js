import {
  Actor,
  CollisionGroupManager,
  ColliderComponent,
  CollisionType,
  Color,
  Engine,
  Entity,
  Shape,
  vec,
  Vector,
} from "excalibur";
import { blocks } from "./resources";

export class OneWayPlatform extends Actor {
  #_sensor;
  constructor(x, y, cols, rows) {
    super({
      name: "Oneway Floor",
      pos: new Vector(x, y),
      anchor: Vector.Zero,
      z: -100,
      width: blocks.cylinder_blue.width * cols,
      height: blocks.cylinder_blue.height * rows,
      color: Color.Red,
      collisionType: CollisionType.Passive,
      collisionGroup: CollisionGroupManager.groupByName("platform"),
    });

    this.cols = cols;
    this.rows = rows;

    for (let i = 0; i < this.cols; i++) {
      for (let j = 0; j < this.rows; j++) {
        this.graphics.show(blocks["cylinder_blue"], {
          anchor: Vector.Zero,
          offset: vec(
            i * blocks["cylinder_blue"].width,
            j * blocks["cylinder_blue"].height
          ),
        });
      }
    }
  }

  onInitialize(_engine) {
    this._sensor = new Actor({
      y: -48,
      collider: Shape.Box(this.width * 2, 1, vec(0.5, 1)),
      collisionGroup: CollisionGroupManager.groupByName("platform"),
    });

    const collider = this._sensor.get(ColliderComponent);
    this.addChild(this._sensor);
    collider.events.on("collisionstart", () => {
      this.body.collisionType = CollisionType.Fixed;
    });
    collider.events.on("collisionend", () => {
      this.body.collisionType = CollisionType.Passive;
    });
  }
}
