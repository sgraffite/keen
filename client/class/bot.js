import * as ex from "excalibur";
import { botSpriteSheet, Resources } from "./resources";
import { Baddie } from "./baddie";

export class Bot extends ex.Actor {
  onGround = true;
  isJumping = false;
  isHurt = false;
  hurtTime = 0;
  constructor(x, y) {
    super({
      name: "Bot",
      pos: new ex.Vector(x, y),
      collisionType: ex.CollisionType.Active,
      collisionGroup: ex.CollisionGroupManager.groupByName("player"),
      collider: ex.Shape.Box(25, 50, ex.Vector.Half, ex.vec(0, 3)),
    });
  }

  // OnInitialize is called before the 1st actor update
  onInitialize(engine) {
    // Initialize actor

    // Setup visuals
    const hurtleft = ex.Animation.fromSpriteSheet(
      botSpriteSheet,
      [0, 1, 0, 1, 0, 1],
      150
    );
    hurtleft.scale = new ex.Vector(2, 2);

    const hurtright = ex.Animation.fromSpriteSheet(
      botSpriteSheet,
      [0, 1, 0, 1, 0, 1],
      150
    );
    hurtright.scale = new ex.Vector(2, 2);
    hurtright.flipHorizontal = true;

    const idle = ex.Animation.fromSpriteSheet(botSpriteSheet, [2, 3], 800);
    idle.scale = new ex.Vector(2, 2);

    const left = ex.Animation.fromSpriteSheet(
      botSpriteSheet,
      [3, 4, 5, 6, 7],
      100
    );
    left.scale = new ex.Vector(2, 2);

    const right = ex.Animation.fromSpriteSheet(
      botSpriteSheet,
      [3, 4, 5, 6, 7],
      100
    );
    right.scale = new ex.Vector(2, 2);
    right.flipHorizontal = true;

    // Register animations with actor
    this.graphics.add("hurtleft", hurtleft);
    this.graphics.add("hurtright", hurtright);
    this.graphics.add("idle", idle);
    this.graphics.add("left", left);
    this.graphics.add("right", right);

    // onPostCollision is an event, not a lifecycle meaning it can be subscribed to by other things
    this.on("postcollision", (evt) => this.onPostCollision(evt));
    //this.on("precollision", (evt) => this.onPreCollision(evt));
  }

  onPreCollision(evt) {
    console.log(evt);
  }

  onPostCollision(evt) {
    // Bot has collided with the Top of another collider
    //console.log(evt.side);
    if (evt.side === ex.Side.Bottom && this.vel.y === 0) {
      this.onGround = true;
    }

    // Bot has collided on the side, display hurt animation
    if (
      (evt.side === ex.Side.Left || evt.side === ex.Side.Right) &&
      evt.other instanceof Baddie
    ) {
      if (this.vel.x < 0 && !this.hurt) {
        this.graphics.use("hurtleft");
      }
      if (this.vel.x >= 0 && !this.hurt) {
        this.graphics.use("hurtright");
      }
      this.isHurt = true;
      this.hurtTime = 1000;
      Resources.hit.play(0.1);
    }
  }

  // After main update, once per frame execute this code
  onPreUpdate(engine, delta) {
    const keyboard = engine.input.keyboard;
    // If hurt, count down
    if (this.hurtTime >= 0 && this.hurt) {
      this.hurtTime -= delta;
      if (this.hurtTime < 0) {
        this.isHurt = false;
      }
    }

    // Reset x velocity
    this.vel.x = 0;

    // Player input
    if (keyboard.isHeld(ex.Keys.Left) || keyboard.isHeld(ex.Keys.A)) {
      this.vel.x = -200;
    }

    if (keyboard.isHeld(ex.Keys.Right) || keyboard.isHeld(ex.Keys.D)) {
      this.vel.x = 200;
    }

    if (
      this.vel.y === 0 &&
      this.onGround &&
      (keyboard.isHeld(ex.Keys.Up) ||
        keyboard.isHeld(ex.Keys.Space) ||
        keyboard.isHeld(ex.Keys.W))
    ) {
      this.vel.y = -450;
      this.onGround = false;
      Resources.jump.play(0.1);
    }

    // Partial jump
    if (
      !this.onGround &&
      this.vel.y < 0 &&
      (keyboard.wasReleased(ex.Keys.Up) ||
        keyboard.wasReleased(ex.Keys.Space) ||
        keyboard.wasReleased(ex.Keys.W))
    ) {
      this.vel.y = this.vel.y / 4;
    }

    // Change animation based on velocity
    if (this.vel.x < 0 && !this.hurt) {
      this.graphics.use("left");
    }
    if (this.vel.x > 0 && !this.hurt) {
      this.graphics.use("right");
    }
    if (this.vel.x === 0 && !this.hurt) {
      this.graphics.use("idle");
    }
  }
}
