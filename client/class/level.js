import * as ex from "excalibur";
import { Baddie } from "./baddie.js";
import { Keen } from "./keen.js";
import { Floor } from "./floor.js";
import { NPC } from "./npc.js";
import { blocks } from "./resources";
import { OneWayPlatform } from "./oneway_platform.js";

export class Level extends ex.Scene {
  constructor() {
    super();
  }

  onInitialize(engine) {
    // Create collision groups for the game
    ex.CollisionGroupManager.create("player");
    ex.CollisionGroupManager.create("enemy");
    ex.CollisionGroupManager.create("floor");
    ex.CollisionGroupManager.create("platform");

    // Compose actors in scene
    const player = new Keen(200, 0);

    const baddies = [];
    baddies.push(new Baddie(engine.halfDrawWidth - 200, 300 - 30, 1));
    baddies.push(new Baddie(engine.halfDrawWidth + 200, 300 - 30, -1));
    //baddies.push(new Baddie(engine.halfDrawWidth + 120, 0 - 30, -1));

    const entities = [];
    entities.push(new NPC(400, 170));
    entities.push(new Floor(0, 300, 20, 1));
    //const floor2 = new Floor(400, 200, 5, 1);
    entities.push(new OneWayPlatform(500, 200, 4, 1));
    entities.push(new OneWayPlatform(500, 232, 4, 1));
    entities.push(new OneWayPlatform(500, 264, 4, 1));

    // Create a tilemap
    const tilemap = new ex.TileMap({
      rows: 10,
      columns: 10,
      tileWidth: 32,
      tileHeight: 32,
      name: "level",
    });

    // loop through tilemap cells
    for (let cell of tilemap.tiles) {
      if (Math.random() > 0.85) {
        cell.addGraphic(blocks["blockSprite"]);
        cell.solid = true;
        cell.collisionGroup = ex.CollisionGroupManager.groupByName("platform");
        cell.collisionType = ex.CollisionType.Fixed;
        cell.addCollider(ex.Shape.Box(32, 32, ex.Vector.Zero));
      } else if (Math.random() > 0.85) {
        cell.addGraphic(blocks["cylinder_blue"]);
        cell.solid = true;
        cell.collisionGroup = ex.CollisionGroupManager.groupByName("platform");
        cell.collisionType = ex.CollisionType.Fixed;
        cell.addCollider(ex.Shape.Box(32, 1, ex.vec(0.5, 1)));
        console.log(cell);
      }
    }

    engine.add(tilemap);
    engine.add(player);
    baddies.forEach((baddie) => engine.add(baddie));
    entities.forEach((entity) => engine.add(entity));

    // For the test harness to be predicable
    if (!window.__TESTING) {
      // Create camera strategy
      this.camera.clearAllStrategies();
      this.camera.strategy.elasticToActor(player, 0.05, 0.1);
    }
  }
}
