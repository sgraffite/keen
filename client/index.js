import * as ex from "excalibur";
import { DevTool } from "@excaliburjs/dev-tools";
import { loader } from "./class/resources";
import { Level } from "./class/level";

const engine = new ex.Engine({
  backgroundColor: ex.Color.fromHex("#5fcde4"),
  // width: 600,
  // height: 400,
  displayMode: ex.DisplayMode.FillScreen,
  fixedUpdateFps: 60,
  // Turn off anti-aliasing for pixel art graphics
  antialiasing: false,
});

//const devtool = new DevTool(engine);

// Set global gravity, 800 pixels/sec^2
ex.Physics.acc = new ex.Vector(0, 800);

// Setup first level as a custom scene
const level = new Level();
engine.add("level", level);
engine.goToScene("level");

// Game events to handle
engine.on("hidden", () => {
  console.log("pause");
  engine.stop();
});
engine.on("visible", () => {
  console.log("start");
  engine.start();
});

// Start the engine
engine.start(loader).then(() => {
  console.log("game start");
});

// For test hook
window.engine = engine;
window.level = level;
